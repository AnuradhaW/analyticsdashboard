import pandas
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA

path = input()

# Read in the data.
csvFile = ""+str(path)
data = pandas.read_csv(csvFile)

# Remove any rows with missing values.
data = data.dropna(axis=0)

# Print the names of the columns in games.
col_names = data.columns
#print(data.columns)
#print(data.shape)

# Make a histogram of all the ratings in the average_rating column.
# plt.hist(data["search_visits"])

# Show the plot.
# plt.show()

# ================================ kmeans model ================================

# Initialize the model with 2 parameters -- number of clusters and random state.
kmeans_model = KMeans(n_clusters=4, random_state=1)

# Get only the numeric columns from data.
good_columns = data._get_numeric_data()

total_rows=len(good_columns.axes[0])

# Fit the model using the good columns.
kmeans_model.fit(good_columns)

# Get the cluster assignments.
labels = kmeans_model.labels_
centroids = kmeans_model.cluster_centers_

#i = 0;
#for label in labels:
#    print("%s : %s" %(data.iloc[i,0],label))
#    i = i + 1

unique_labels = set(labels)
no_clusters = unique_labels.__len__()
#print(no_clusters)

strs = ["" for x in range(no_clusters)]

for x in range(0, no_clusters):
    strs[x] += str(x) + "" + str(x) + ": "

output = "centroids: "

for ar in centroids:
    k = 1
    for centroid in ar:
        output += str(col_names[k]) + ":" + str(centroid) + " "
        k = k + 1
    output += "; "

output += "|"

#output += str(centroids)

# ================================ scatter plot ================================

# Create a PCA model.
pca_2 = PCA(2)

# Fit the PCA model on the numeric columns from earlier.
plot_columns = pca_2.fit_transform(good_columns)
plot_centroids = pca_2.fit_transform(centroids)

colors = 10*['r.','g.','b.','c.','k.','y.','m.']

for i in range(len(plot_columns)):
    plt.plot(plot_columns[i][0], plot_columns[i][1], colors[labels[i]], markersize = 10)

k = 0;
for i in range(len(plot_centroids)):
    plt.plot(plot_centroids[i][0], plot_centroids[i][1], colors[k], markersize = 10, marker="x", zorder=10)
    k = k + 1

# Make a scatter plot of each game, shaded according to cluster assignment.
#plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1], c=labels)
#plt.scatter(x=plot_centroids[:,0], y=plot_centroids[:,1], marker="x", s=150, linewidths=5, zorder=10)

x_values = plot_columns[:,0]
y_values = plot_columns[:,1]

#output += "x: "
#for x in x_values:
#    output += str(x) + " "

#output += "y: "
#for y in y_values:
#    output += str(y) + " "
	
#output += "labels: "
#for label in labels:
#    output += str(label) + " "
#output += ";"

#print(output)

# =================================================== new =====================================================

for x in range(0, total_rows):
    strs[labels[x]] += "[" + str(x_values[x]) + "," + str(y_values[x]) + "] "


for s in strs:
    output += s

output += "|"

print(output)

# =============================================================================================================

# Show the plot.
#plt.show()

#i = 0;
#for x in x_values:
#    print("[%s , %s]" %(x,y_values[i]))
#    i = i + 1

# Get xy data used to plot the graph
#plt.plot([1,2,3],[4,5,6])
#ax = plt.gca() # get axis handle
#line = ax.lines[0] # get the first line, there might be more
#print(line.get_xydata())