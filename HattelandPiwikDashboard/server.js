var port = 1338;
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var fs = require('fs');
var request = require('request');
var ip="52.90.200.80";

var PiwikClient = require('piwik-client');  // a core module
var myClient = new PiwikClient("http://"+ip+"///piwik/index.php", 'bf8535dfb2511c611da95bd9a8e63e57');

var PythonShell = require('python-shell');

//app.use('/js', express.static(__dirname + '/client/js'));
app.use(express.static(__dirname + "/client"));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/client/views/index.html');
});

app.get('/Files/GetBatch/:WEBSITEID', function (req, res) {
    var websiteid = req.params.WEBSITEID;
    res.sendFile(__dirname + '/client/data/'+websiteid+'/batch/batch.csv');
});

app.get('/Files/textrecord.json', function (req, res) {
    res.sendFile(__dirname + '/client/python/textrecord.json');
});

app.get('/Files/result.txt', function (req, res) {
    res.sendFile(__dirname + '/client/python/result.txt');
});

app.listen(port, function () {  // keep on listening on this port and handle http requests
    console.log('I\'m listening on port: '+port);
});

app.get('/dowork',function(req, res){

  //var id = req.params.ID;

  //console.log('came to dowork '+id);

  // fs.writeFile('./client/python/result.txt','',function(err){
  //   if(err)
  //     console.error(err);
  //    //console.log('Written!');
  // });

  //  var pyshell = new PythonShell('./client/python/AWS-boto3.py');
   
  // pyshell.on('message', function (message) {
  //  fs.appendFile('./client/python/result.txt',message,function(err){
  //    if(err)
  //      console.error(err);
  //    console.log('Appended!');
  //  });
  // });
   
  // pyshell.end(function (err) {
  //   if (err) throw err;
  //   console.log('finished');
  // });

  // PythonShell.run('./client/python/AWS-boto3.py', function (err) {
  //   if (err) {
  //     console.log(err);
  //   }
  //   //console.log('finished');
  // });

  // res.write('success');
  // res.end();

  var pyshell = new PythonShell('./client/python/AWS-boto3.py');
     
  pyshell.on('message', function (message) {
    console.log(message);
    res.write(message);
    res.end();
  });
   
  pyshell.end(function (err) {
    if (err){
      console.log(err);
    } else {
      console.log('finished giving profile');
    }
  });
  //res.end();
});

app.get('/getClusterData/:WEBSITEID',function(req, res){

  var websiteid = req.params.WEBSITEID;

  var pyshell = new PythonShell('./client/admin_notify/testKMeans.py');

  //pyshell.send(websiteid);
  pyshell.send("./client/data/" + websiteid + "/batch/batch_1000.csv");
     
  pyshell.on('message', function (message) {

    processPythonClusterOutput(message,res);
    
  });
   
  pyshell.end(function (err) {
    if (err){
      console.log(err);
      res.write("error");
      res.end();
    } else {
      console.log('finished giving cluster data');
    }
  });

});

/* ================================ End point to get timely cluster data of the index block ================================ */

app.get('/getTimelyClusterData/:WEBSITEID/:INDEX',function(req, res){

  var websiteid = req.params.WEBSITEID;
  var index = req.params.INDEX;

  var pyshell = new PythonShell('./client/admin_notify/testKMeans.py');

  //pyshell.send(websiteid);
  pyshell.send("./client/data/" + websiteid + "/timelyBatches/" + index + ".csv");
     
  pyshell.on('message', function (message) {

    processPythonClusterOutput(message,res);
    
  });
   
  pyshell.end(function (err) {
    if (err){
      console.log(err);
      res.write("error");
      res.end();
    } else {
      console.log('finished giving cluster data');
    }
  });

});

function processPythonClusterOutput(message,res) {

  var output = message;

  var ar1 = output.split(" ; |");

  var centroidObjAr = [];
  

  var ar11 = ar1[0].split("centroids: ");
  //ar11[1] // centroids
  var centArr = ar11[1].split(" ; ");

  var cent_no = 0;

  for (var k in centArr) {
    var attributes = centArr[k].split(" ");

    var treemapSeriesChildren = [];

    var suffix = "_sum_event_value";

    for(var n in attributes) {
      var values = attributes[n].split(":");

      if(values[0].indexOf(suffix, values[0].length - suffix.length) !== -1) {
        treemapSeriesChildren.push({"text":values[0],"value": Number(values[1])});
      }

    }

    var jsonObj = {
      "type":"treemap",
      "series":[
          {
              "text":"Centroid "+cent_no,
              "children":treemapSeriesChildren
          }
      ]
    }
    centroidObjAr.push(jsonObj);
    cent_no += 1;
  }

  var seriesObjAr = [];

  var rest = ar1[1];

  for (i = 0; i < centArr.length; i++) { 

      console.log(i);

      var s = [];

      if(i == 0) {
        s = rest.split(""+i+""+i+": ");  ////
      } else {
        s = rest.split(""+i+": ");
      }
      
      var want_s = [];
      if(i == (centArr.length - 1)) {
        want_s = s[1].split(" |"); 
      } else {
        want_s = s[1].split(" "+(i+1)+"");   /////
      }

      var cordinateArr = want_s[0].split(" ");

      rest = want_s[1];

      var cordinates = [];
      for(var c in cordinateArr) {
        var cordinateStr = cordinateArr[c];
        var a1 = cordinateStr.split("[");
        var a2 = a1[1].split("]");
        var a3 = a2[0].split(",");
        var x_cordinate = Number(a3[0]);
        var y_cordinate = Number(a3[1]);
        cordinates.push([x_cordinate,y_cordinate]);
      }

      seriesObjAr.push({values:cordinates});

      console.log(i);
  }

  var jsonData = {
    no_of_clusters: centArr.length,
    centroids: centroidObjAr,
    series: seriesObjAr
  }

  if(res != null) {
    res.json(jsonData);
    res.end();
  } else {
    return jsonData;
  }

}

app.post('/writeData',function(req, res){
    //console.log(req.body);
    var data = JSON.stringify(req.body);
    //var data = '{"Id":"1","Elevation":"2596.0","Aspect":"51.0","Slope":"3.0","Horizontal_Distance_To_Hydrology":"258.0","Vertical_Distance_To_Hydrology":"0.0","Horizontal_Distance_To_Roadways":"510.0","Hillshade_9am":"221.0","Hillshade_Noon":"232.0","Hillshade_3pm":"148.0","Horizontal_Distance_To_Fire_Points":"6279.0","Wilderness_Area1":"1.0","Wilderness_Area2":"0.0","Wilderness_Area3":"0.0","Wilderness_Area4":"0.0","Soil_Type1":"0.0","Soil_Type2":"0.0","Soil_Type3":"0.0","Soil_Type4":"0.0","Soil_Type5":"0.0","Soil_Type6":"0.0","Soil_Type7":"0.0","Soil_Type8":"0","Soil_Type9":"0.0","Soil_Type10":"0.0","Soil_Type11":"0.0","Soil_Type12":"0","Soil_Type13":"0.0","Soil_Type14":"0.0","Soil_Type15":"0.0","Soil_Type16":"0","Soil_Type17":"0.0","Soil_Type18":"0","Soil_Type19":"0","Soil_Type20":"0","Soil_Type21":"0.0","Soil_Type22":"0.0","Soil_Type23":"0","Soil_Type24":"0","Soil_Type25":"0.0","Soil_Type26":"0.0","Soil_Type27":"0.0","Soil_Type28":"0.0","Soil_Type29":"1","Soil_Type30":"0","Soil_Type31":"0.0","Soil_Type32":"0.0","Soil_Type33":"0.0","Soil_Type34":"0.0","Soil_Type35":"0.0","Soil_Type36":"0.0","Soil_Type37":"0.0","Soil_Type38":"0.0","Soil_Type39":"0.0","Soil_Type40":"0"}';
    fs.writeFile('./client/python/testrecord.json',data,function(err) {
      if(err)
        console.error(err);
      console.log('Written to ./client/python/textrecord.json!');
      res.write("success");
      res.end();
    });
});

app.post('/writeBatchData/:WEBSITEID',function(req, res) {
    var websiteid = req.params.WEBSITEID;
    var data = req.body.batch;

    fs.writeFile('./client/data/'+websiteid+'/batch/batch.csv',data,function(err){
      if(err)
        console.error(err);
      res.write('success');
      res.end();
    });
});

app.post('/writeTimelyBatchData/:WEBSITEID/:INDEX',function(req, res) {
    var websiteid = req.params.WEBSITEID;
    var index = req.params.INDEX;
    var data = req.body.batch;

    fs.writeFile('./client/data/'+websiteid+'/timelyBatches/'+index+'.csv',data,function(err){
      if(err)
        console.error(err);
      res.write('success');
      res.end();
    });
});

app.get('/getProfile/:WEBSITEID/:USERID',function(req, res){

    var websiteid = req.params.WEBSITEID;
    var userid = req.params.USERID;

    var createdDate = '2015-12-01';
    //var globalEvents = ["search","bookflights","popular","reserve","discount"];
    var globalEvents = [];

    fs.readFile('./client/data/'+websiteid+'/json/layout.json', 'utf8', function (err, data) {
      if (err) {
        throw err;
      } else {
        var jsonData = JSON.parse(data);

        var rows = jsonData.rows;
        for(var k in rows) {
            var cols = rows[k].cols;
            for(var m in cols) {
                var eventCategory = cols[m].feature.eventCategory;
                globalEvents.push(eventCategory);
                console.log(eventCategory);
            }
        }

        myClient.api({
            method: 'Events.getCategory',
            idSite: websiteid,
            period: 'range',
            date: ''+createdDate+',today',
            segment: 'userId==' + userid
        }, function (err, responseObject) {
            if (!err) {
                var resultStr = JSON.stringify(responseObject);
                console.log(resultStr);

                if(resultStr == "[]") {
                  res.write("0");
                  res.end();
                  return;
                }

                var jsonData = '{"Id":"'+userid+'",';

                var len = globalEvents.length - 1;

                // write json data to file to be sent to AWS
                for(var k in globalEvents) {
                    var found = false;
                    var e = globalEvents[k];
                    for(var m in responseObject) {
                        if(globalEvents[k] == responseObject[m].label) {
                            jsonData += '"'+e+'_visits":"'+responseObject[m].nb_visits+'","'+e+'_events":"'+responseObject[m].nb_events+'","'+e+'_events_with_value":"'+responseObject[m].nb_events_with_value+'","'+e+'_sum_event_value":"'+responseObject[m].sum_event_value+'","'+e+'_min_event_value":"'+responseObject[m].min_event_value+'","'+e+'_max_event_value":"'+responseObject[m].max_event_value+'","'+e+'_sum_daily_nb_uniq_visitors":"'+responseObject[m].sum_daily_nb_uniq_visitors+'","'+e+'_avg_event_value":"'+responseObject[m].avg_event_value+'"';
                            if(k!=len) {
                                jsonData += ',';    
                            }
                            found = true;
                            break;
                        }
                    }
                    if(!found) {
                        jsonData += '"'+e+'_visits":"0","'+e+'_events":"0","'+e+'_events_with_value":"0","'+e+'_sum_event_value":"0","'+e+'_min_event_value":"0","'+e+'_max_event_value":"0","'+e+'_sum_daily_nb_uniq_visitors":"0","'+e+'_avg_event_value":"0"';
                        if(k!=len) {
                            jsonData += ',';  
                        }
                    }
                }

                jsonData += '}';

                /////////////// start write data ////////////////

                var data = JSON.stringify(req.body);

                fs.writeFile('./client/endpoint/testrecord.json',jsonData,function(err){

                  if(err) {
                    console.error(err);
                  } else {
                    console.log('Written to /endpoint/textrecord.json!');
                  
                    //////////////// execute python scipt ////////////////////

                    var pyshell = new PythonShell('./client/endpoint/AWS-boto3.py');

                    pyshell.on('message', function (message) {
                      console.log(message);
                      res.write(message);
                      res.end();
                    });
                     
                    pyshell.end(function (err) {
                      if (err){
                        console.log(err);
                      } else {
                        console.log('finished simulation of endpoint');
                      }
                    });
                  }
                });

            } else {
                console.log(err);
            }
        });
      }
    });

});

/* ======================================= Getting data from Piwik Reporting API ======================================= */

app.get('/getPiwikData/:METHOD/:IDSITE/:PERIOD/:DATE/:SEGMENT',function(req, res){

  var methodVal = req.params.METHOD;
  var idsiteVal = req.params.IDSITE;
  var periodVal = req.params.PERIOD;
  var dateVal = req.params.DATE;
  var segmentVal = req.params.SEGMENT;

  getPiwikData(methodVal,idsiteVal,periodVal,dateVal,segmentVal,res);

  console.log(methodVal);

});

function getPiwikData(methodVal,idsiteVal,periodVal,dateVal,segmentVal,res) {
  if(segmentVal == '""') {
    myClient.api({
      method: methodVal,
      idSite: idsiteVal,
      period: periodVal,
      date: dateVal
    }, function (err, responseObject) {
        if (!err) {
          res.json(responseObject);
          res.end();
        } else {
            console.log(err);
        }
    });    
  } else {
    myClient.api({
      method: methodVal,
      idSite: idsiteVal,
      period: periodVal,
      date: dateVal,
      segment: segmentVal
    }, function (err, responseObject) {
        if (!err) {
          res.json(responseObject);
          res.end();
        } else {
            console.log(err);
        }
    });
  }
}

/* ======================================= Generating timely data clusters ==================================== */

app.get('/getTimelyClusters/:WEBSITEID/:TIME',function(req, res){

  var websiteid = req.params.WEBSITEID;
  var time = req.params.TIME;

  var timelyUniqueUserIds = [];

  var method = 'Live.getLastVisitsDetails';
  var idSite = websiteid;
  var period = 'month';
  var date = 'today';

  var date = new Date();
  var thisMonth = date.getMonth() + 2;  // 1 = Jan
  var thisYear = date.getFullYear();  // 2016

  var index = 0;

  var monthDatesArray = [];

  for (i = 0 ; i < time; i++) {

      if ( (thisMonth - 1) >= 1 ) {
        thisMonth = thisMonth - 1;
      } else if ( (thisMonth - 1) == 0 ) {
        thisMonth = 12;
        thisYear = thisYear - 1;
      }

      var d = new Date(thisYear, thisMonth, 0);
      var day = d.getDate();
      var monthIndex = d.getMonth() + 1;
      var year = d.getFullYear();
      var dateNew = ''+year+'-'+monthIndex+'-'+day;

      monthDatesArray.push(dateNew);

      request('http://localhost:1338/getPiwikDataSup/'+method+'/'+idSite+'/'+period+'/'+dateNew+'/""/'+index+'', function (error, response, body) {
        if (!error && response.statusCode == 200) {
          var jsonData = JSON.parse(body);
          console.log(jsonData.index);

          var visitorLogs = jsonData.response;
          var userIds = [];

          for(var k in visitorLogs) {
              var len = visitorLogs.length - 1;
              var id = visitorLogs[k].userId;
              if(userIds.indexOf(id) == -1) {
                  userIds.push(id);
              }
          }

          //timelyUniqueUserIds.push(userIds);
          timelyUniqueUserIds.splice(jsonData.index, 0, userIds);

          if(timelyUniqueUserIds.length == time) {

            var globalEvents = [];

            fs.readFile('./client/data/'+websiteid+'/json/layout.json', 'utf8', function (err, data) {

              if (err) {
                throw err;
              } else {
                var layout = JSON.parse(data);
                var rows = layout.rows;
                for(var k in rows) {
                  var cols = rows[k].cols;
                  for(var m in cols) {
                      var eventCategory = cols[m].feature.eventCategory;
                      globalEvents.push(eventCategory);
                  }
                }
                
                var clusterData = [];
                for(var n in timelyUniqueUserIds) {
                  //console.log("============================================== " + websiteid +" "+timelyUniqueUserIds[n].join() +" "+time + " "+monthDatesArray[n] + " "+n+" "+globalEvents.join());
                  writeTimelyDataFiles(websiteid, timelyUniqueUserIds[n], time, monthDatesArray[n], n, globalEvents, clusterData, res);
                }
                
              }
            });
            
          }

         } else {
            console.log(error);
         }
      });

      index++;   
  }

});

function writeTimelyDataFiles(websiteid, userIds, time, monthDate, index, globalEvents, clusterData, res) {
     
      /* Generate batch data files for each time block */
      var csvData = "Id";
      for(var k in globalEvents) {
          var e = globalEvents[k];
          csvData += ","+e+"_visits,"+e+"_events,"+e+"_events_with_value,"+e+"_sum_event_value,"+e+"_min_event_value,"+e+"_max_event_value,"+e+"_sum_daily_nb_uniq_visitors,"+e+"_avg_event_value";
      }
      csvData += "\n";
      var rowUserIdIndex = 0;

      recursuveCall(0, csvData, userIds, monthDate, websiteid, index, globalEvents, time, clusterData, res);

}

function recursuveCall(rowUserIdIndex, csvData, userIds, monthDate, websiteid, index, globalEvents, time, clusterData, res) {

  if (userIds.length == 0) {

    console.log("zero user ids");

    //console.log(index +" ============== "+ csvData);

    var jsonBody = {
      no_of_clusters: 0,
      centroids: [],
      series: []
    };

    clusterData.splice((time - index -1), 0, jsonBody);
    console.log("pushed to cluster Data " + index);
    if(clusterData.length == time) {

      sendTimelyClusterResponse(res, clusterData, time);

    }

  } else {

    var segmentVar = 'userId==' + userIds[rowUserIdIndex];

    console.log(segmentVar);

    var method = 'Events.getCategory';
    var idSite = websiteid;
    var period = 'month';
    var date = ''+monthDate;

    request('http://localhost:1338/getPiwikData/'+method+'/'+idSite+'/'+period+'/'+date+'/'+segmentVar+'', function (error, response, body) {
        
      if (!error && response.statusCode == 200) {  
        //console.log("second: "+segmentVar);

        var data = JSON.parse(body);
        //console.log(data);

        csvData += ""+userIds[rowUserIdIndex];

        var len = userIds.length;

        // write data to csv batch file to be sent to AWS
        for(var k in globalEvents) {
            var found = false;
            var e = globalEvents[k];
            for(var m in data) {
                if(globalEvents[k] == data[m].label) {
                    csvData += ","+data[m].nb_visits+","+data[m].nb_events+","+data[m].nb_events_with_value+","+data[m].sum_event_value+","+data[m].min_event_value+","+data[m].max_event_value+","+data[m].sum_daily_nb_uniq_visitors+","+data[m].avg_event_value;
                    found = true;
                    break;
                }
            }
            if(!found) {
                csvData += ",0,0,0,0,0,0,0,0";
            }
        }

        csvData += "\n";

        rowUserIdIndex++;

        if(rowUserIdIndex == len) {

          request({
            url: 'http://localhost:1338/writeTimelyBatchData/'+websiteid+'/'+index+'', //URL to hit
            qs: {from: 'blog example', time: +new Date()}, //Query string data
            method: 'POST',
            //Lets post the following key/values as form
            json: {
              "batch":csvData
            }
          }, function(error, response, body){
            if(error) {
                console.log(error);
            } else {
                console.log(response.statusCode, body);

                request('http://localhost:1338/getTimelyClusterData/'+websiteid+'/'+index+'', function (error, response, body) {
                  if (!error && response.statusCode == 200) {  
                    clusterData.splice((time - index -1), 0, JSON.parse(body));
                    console.log("pushed to cluster Data "+index);

                    if(clusterData.length == time) {
                      sendTimelyClusterResponse(res, clusterData, time);
                    }

                  } else {
                    console.log("Error");
                  }
                });

            }
          }); 

          console.log(index +" ============== "+ csvData);
            
        } else {
            recursuveCall(rowUserIdIndex,csvData,userIds,monthDate,websiteid,index,globalEvents,time,clusterData,res);
        }
      } else {
        console.log("error");
      }
    });

  }
              
}

function sendTimelyClusterResponse(res, clusterData, time) {

  var bubbleNoOfClustersData = [];
  var bubbleNoOfClustersValues = [];

  var max_no_of_clusters = 0;

  for(var j = 0; j < time; j++) {
    var n = clusterData[j].no_of_clusters;
    bubbleNoOfClustersValues.push([(j+1),10,n]);
    if(n > max_no_of_clusters) {
      max_no_of_clusters = n;
    }
  }

  bubbleNoOfClustersData.push({
    values: bubbleNoOfClustersValues
  });

  var bubbleData = [];

  for(var i = 0; i < max_no_of_clusters; i++) {   // i == current cluster
    var valueArray = [];
    for(var j = 0; j < time; j++) {   // j == time block

      var x = (j+1);
      var y = (i+1)*10;
      var weight = 0;

      if(clusterData[j].series.length > i) {
        weight = clusterData[j].series[i].values.length;
      }

      valueArray.push([x,y,weight]);

    }
    bubbleData.push({
      values: valueArray
    });
  }

  res.json({
    bubbleNoOfClustersSeries: bubbleNoOfClustersData,
    bubbleDataSeries: bubbleData,
    clusterDataArray: clusterData
  });
  res.end();

}

/* support endpoint for generating timely data */

app.get('/getPiwikDataSup/:METHOD/:IDSITE/:PERIOD/:DATE/:SEGMENT/:INDEX',function(req, res){

  var methodVal = req.params.METHOD;
  var idsiteVal = req.params.IDSITE;
  var periodVal = req.params.PERIOD;
  var dateVal = req.params.DATE;
  var segmentVal = req.params.SEGMENT;
  var index = req.params.INDEX;

  getPiwikDataSup(methodVal,idsiteVal,periodVal,dateVal,segmentVal,index,res);

  console.log(methodVal);

});

function getPiwikDataSup(methodVal,idsiteVal,periodVal,dateVal,segmentVal,ind,res) {
  if(segmentVal == '""') {
    myClient.api({
      method: methodVal,
      idSite: idsiteVal,
      period: periodVal,
      date: dateVal
    }, function (err, responseObject) {
        if (!err) {
          res.json({index:ind, response:responseObject});
          res.end();
        } else {
            console.log(err);
        }
    });    
  } else {
    myClient.api({
      method: methodVal,
      idSite: idsiteVal,
      period: periodVal,
      date: dateVal,
      segment: segmentVal
    }, function (err, responseObject) {
        if (!err) {
          res.json({"index":index, "response":responseObject});
          res.end();
        } else {
            console.log(err);
        }
    });
  }
}